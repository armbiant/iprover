import subprocess
import atexit
import config
from time import sleep
import os
import re
import time
import sys
import helper as hp
from config import PROVER_PROCESSES

# Set the root logger
import logging
log = logging.getLogger()


# Object for storing prover processes
class ProverProc(object):

    def __init__(self, proc, output_file, start_time, max_runtime, heur_id="id none"):
        self.proc = proc
        self.output_file = output_file
        self.start_time = start_time
        self.max_runtime = max_runtime
        self.heur_id = heur_id
        #self.cmd_string = cmd_string

    def set_identifier(self, heur_id):
        self.heur_id = heur_id
        return self

    def __str__(self):
        return str(self.heur_id)


# Function for returning whether a terminated prover run was successfull or not
def check_prover_success(szs_status, ltb_mode):

    # LTB mode check (uses axiom selector)
    if ltb_mode:
        if re.match('(Theorem|Unsatisfiable)', szs_status):
            return True
        else:
            return False
    # Standard zsz ontology success check
    else:
        if re.match('(Theorem|Unsatisfiable|Satisfiable|CounterSatisfiable)', szs_status):
            return True
        else:
            return False


# Get the running time and szs status of the output of a terminated prover run.
# The expected prover output is a bytestring.
def get_prover_output_status(prover_out):

    try:
        # Get the SZS status
        szs_status = re.search('% SZS status.*', prover_out).group(0)
        szs_status = szs_status.split()[3]

    except AttributeError:
        szs_status = "Stopped"

    return szs_status


def handle_finished_prover_run(prover_proc):
    # Handle terminated prover by inspecting the output and aquire the szs status

    # Make sure process is all finished
    try:
        _, err = prover_proc.proc.communicate(timeout=10)  # timeout as the clausifier can hang
    except subprocess.TimeoutExpired:
        hp.kill_prover_process(prover_proc)
        _, err = prover_proc.communicate()

    #  Check if any errors
    with open(prover_proc.output_file + "_error", 'r') as f:
        err = f.read()
        if (err != '\n\n' and err != '') or prover_proc.proc.returncode != 0:
            err = err.strip()
            log.error("Prover process \"{0}\" ran with exitcode {1} and error: {2}".format(prover_proc, prover_proc.proc.returncode, err))

    # Prover run exited without error, yet it may take some time for the proof output
    # to be written to the output file. Therefore, try a few times if SZS status is not
    # in the output.
    MAX_READ_TRIES = 3
    for try_no in range(MAX_READ_TRIES):

        # Read the proof output
        with open(prover_proc.output_file, 'r') as f:
            prover_out = f.read()

        # Parse the proof output
        szs_status = get_prover_output_status(prover_out)
        log.debug("Read status {0} on try number {1}".format(szs_status, try_no))
        if szs_status != "Stopped":
            log.debug("Found non-Stopped status. Breaking SZS read loop.")
            break
        else:
            # Wait as we assume the prover is still piping output.
            log.debug("Did not find a good SZS status. Sleeping")
            time.sleep(0.3)

    # If we still could not get a result from the solver, we set status to unknown
    if szs_status == "Stopped":
        log.info("Tried to read SZS status from output {0} times. Setting status as Unknown".format(MAX_READ_TRIES))
        szs_status = "Unknown"

    # Return run status
    return szs_status

def overkill_process(prover_proc):
    # Possibly "Overkill"
    hp.kill_prover_process(prover_proc)
    _ = prover_proc.proc.communicate()  # Empty buffer
    log.info("Forcefully Terminated Process: {0}".format(prover_proc))


# This function looks for terminated processes, and if they exist handles them.
# If a successfull prover run is found, the function terminates all other processes.
def checkTerminatedProcesses(provers_running, ltb_mode):

    # Loop through all the processes currently running
    proc_list = list(PROVER_PROCESSES.items())
    for dict_index, prover_proc in proc_list:
        process_terminated = False

        # If process has finished
        if prover_proc.proc.poll() is not None:
            log.info("Process {0} has terminated".format(prover_proc))
            process_terminated = True

        # Process has not finished but it should have timed out according to the timeout
        # we gave it. We include a 3 second grace here
        elif 3.0 + prover_proc.start_time + prover_proc.max_runtime < time.time():
            log.info("Process {0} has ran past grace.".format(prover_proc))

            # Before terminating we check if the SZS status exists in the prover output
            # in case the printing/reconstruction is taking a lot of time
            with open(prover_proc.output_file, 'r') as f:
                prover_out = f.read()
            szs = get_prover_output_status(prover_out)
            if check_prover_success(szs, ltb_mode):
                log.info("Proof attempt is succesfull but lagging. Waiting for completion")
                print("% SZS status {0}".format(szs)) # Make calling script know

                # Make sure proof file is outputted in case of external interruption
                atexit.register(hp.output_proof, prover_proc.output_file)
                log.debug("Registered output_proof function with argument: {0}".format(prover_proc.output_file))

                # Keep polling the process until it eventually terminates
                while prover_proc.proc.poll() is None:
                    log.debug("Waiting for successfull attempt to terminate ...")
                    time.sleep(0.3)

                # Process has finally terminated
                process_terminated = True
                log.info("Process finally terminated successfully. Getting ready to process")

            else:
                log.info("Process {0} was unsuccessfull, killing.".format(prover_proc))
                overkill_process(prover_proc)
                process_terminated = True # This handles the removal from dict and double checks the SZS status

        # If the process terminated, check the output for successfull results
        if process_terminated:

            # Remove process from the dictionary
            del PROVER_PROCESSES[dict_index]
            # Decrement the number of processes running
            provers_running -= 1

            # Parse output from prover run and check SZS status
            szs_status = handle_finished_prover_run(prover_proc)
            log.info("Finished: {0} with status {1}".format(prover_proc, szs_status))

            # If the attempt was succesfull, handle it.
            # Otherwise, delete the output file and continue checking the other processes
            if check_prover_success(szs_status, ltb_mode):
                log.info('Terminating process loop due to success of {0}'.format(prover_proc))

                # Register the proof output to make sure there are no last minute mishaps
                atexit.register(hp.output_proof, prover_proc.output_file)

                # Return prover has terminated with success
                return provers_running, True, prover_proc.output_file
            else:
                # Delete the file as it doesn´t contain any proofs
                os.remove(prover_proc.output_file)
                os.remove(prover_proc.output_file + "_error")

    # No succesfull attempts yet
    return provers_running, False, ""


def check_if_last_call(no_cores, total_attempts, this_attempt_no):
    # The last processes to start on a core is the last call.
    # This is used to determine whether a process should run for its
    # assigned time, or for the time left, according to what best maximises
    # the time available.
    return total_attempts - this_attempt_no <= no_cores


def run_schedule(problem, schedule_mode, heuristic_spec, time_limit, no_cores, ltb_mode=False, prep_heur=None):

    # We use the PROVER_PROCESSES dictionary from config to store all the running processes

    # Set the start time
    start_time = time.time()

    # The current heuristic index
    heuristic_index = 0

    # The number of provers/processes currently running
    provers_running = 0

    # Represents whether a prover has found a proof
    success = False

    # Start running prover processes for each core available.
    # We continue to start new prover processes while:
    #   - There are more heuristics left
    #   - The timeout for the problem is not met
    #   - A process has not terminated with success
    while heuristic_index < len(heuristic_spec) and time.time() - \
            start_time < time_limit and not success:

        # Compute the time left
        time_left = time_limit - (time.time() - start_time)
        log.debug("Time left: {0:.2f}".format(time_left))


        # Compute whether the next process is a last call, -> should run until assigned completion
        last_call = check_if_last_call(no_cores, len(heuristic_spec), heuristic_index)
        log.debug("Last call: {0}".format(last_call))

        # Build the cmd string according to the schedule
        if schedule_mode == "external":
            cmd_string, max_run_time, heur_id = build_prover_cmd_external(
                problem, time_left, heuristic_spec[heuristic_index], last_call=last_call)
        elif schedule_mode == "internal":
            cmd_string, max_run_time, heur_id = build_prover_cmd_internal(
                problem, time_left, heuristic_spec[heuristic_index], prep_heur, last_call=last_call)
        else:
            log.error("Mode {0} not defined for build_prover_cmd {0}".format(schedule_mode))
            sys.exit(1)

        # Start the prover process
        log.info("Starting prover on {0} for time {1:.2f}".format(heuristic_spec[heuristic_index], max_run_time))
        log.debug("cmd_string {0}".format(cmd_string))

        prover_proc = start_prover_process(cmd_string, max_run_time)
        # Set the id for recognition later
        prover_proc.set_identifier(heur_id)

        # Store the prover process in a process dict
        PROVER_PROCESSES[str(heuristic_index)] = prover_proc

        # Increment to the next heuristic
        heuristic_index += 1

        # Increment the number of provers running
        provers_running += 1

        # If we are utilising all the cores, have no successfull proofs and have not met the timelimit
        # we sleep until the above is not satisfied (most likely a prover terminates)
        while (provers_running >= no_cores or heuristic_index == len(heuristic_spec)
               ) and not success and time.time() - start_time < time_limit and provers_running > 0:

            log.debug("Time used: {0:.2f}".format(time.time() - start_time))
            # Set program to sleep
            sleep(config.PROC_SLEEP_TIME)

            # Check if some processes have terminated
            provers_running, success, proof_file = checkTerminatedProcesses(provers_running, ltb_mode)

    # Return success
    return success, proof_file


def start_prover_process(cmd_string, max_runtime):

    # Direct stdout to a temporary file
    out_file = hp.get_tmp_out_file()
    cmd_string += '  1>> {0} 2>> {1}_error'.format(out_file, out_file)
    log.debug("Running cmd: {0}".format(cmd_string))

    # Start the prover process
    proc = subprocess.Popen(cmd_string, shell=True, preexec_fn=os.setsid)

    # Store the information in a prover process object
    prover_proc = ProverProc(proc, out_file, time.time(), max_runtime)

    return prover_proc


def get_heuristic_string(heur_file, timeout, opt_filter=[]):

    # Get the heuristic from the config file if none is given
    # Open prover configuration and get heuristic

    # Read heuristic and make each option a list entry
    with open(heur_file, "r") as fp:
        op = fp.read().splitlines()

    # Add quatations to the list options and the clausifier options
    for n in range(len(op)):

        # Quotation values
        # First check nested queue
        if "[[" in op[n] and "]]" in op[n]:
            op[n] = op[n].replace("[[", "\"[[")
            op[n] = op[n].replace("]]", "]]\"")
        # Check normal lists
        elif "[" in op[n] and "]" in op[n]:
            op[n] = op[n].replace("[", "\"[")
            op[n] = op[n].replace("]", "]\"")
        elif "--clausifier_options" in op[n]:
            # Quote the clausifier options
            op[n] = op[n][:21] + "\"" + op[n][21:] + "\""
            # Change the timeout for the clausifier
            if "-t" in op[n]:
                op[n] = re.sub("-t [0-9]+([,.][0-9]+)?", "-t {0}".format(timeout), op[n])

    # Always filter out the timeout
    opt_filter += ["--time_out_real"]

    # If we are not reconstructing the proof, make sure we have all related options removed
    # to remove any duplications.
    log.debug("PROOF_MODEL_OUT is set to: {0}".format(config.PROOF_MODEL_OUT))
    if not config.PROOF_MODEL_OUT:
        opt_filter += ['--inst_out_proof', '--res_out_proof', '--sat_out_model']

    # Remove context parameters if set
    context = config.CONTEXT_MODIFIERS[config.HEURISTIC_CONTEXT]
    if len(context) > 0:
        log.debug("Removing parameters to prep for context: {0}".format(config.HEURISTIC_CONTEXT))
        opt_filter += [param.split()[0] for param in context]

    # Remove entries which contains the filter index
    for opt in opt_filter:
        for n in range(len(op)):
            if opt in op[n]:
                del op[n]
                break  # Found the options, so exit

    # Concatenate all options as one string
    op = " ".join(op)

    # Suppress the proof reconstruction
    if not config.PROOF_MODEL_OUT:
        op += " --inst_out_proof false --res_out_proof false --sat_out_model none "

    # Add the context parameters
    if len(context) > 0:
        log.debug("Adding parameters for context: {0}".format(config.HEURISTIC_CONTEXT))
        op += " " + " ".join(context)

    return op


def build_prover_cmd_external(problem, time_left, heuristic_spec, last_call=False, check_syntax=False):

    # Compute the running time for this process
    heur_timelimit = heuristic_spec[1]
    if last_call or heur_timelimit is None:
        timeout = time_left
    else:
        timeout = min(heur_timelimit, time_left)

    # Get the heuristic in cmd string format
    heur_path = heuristic_spec[0]
    heur_string = get_heuristic_string(heur_path, timeout)

    # Set prover, heuristic and timeout
    if check_syntax:
        cmd_string = "{0} {1} --time_out_real {2:.2f} --dbg_just_parse true {3}".format(config.PROVER, heur_string, timeout, problem)
    else:
        cmd_string = "{0} {1} --time_out_real {2:.2f} {3}".format(config.PROVER, heur_string, timeout, problem)

    # Build a string identifier
    heur_id = "{0};{1:.2f}".format(heur_path, timeout)

    return cmd_string, timeout, heur_id


def build_prover_cmd_internal(problem, time_left, schedule_spec, prep_heuristic, last_call=False):

    # Start with a runtime of 0 and incrementally build up
    timeout = 0

    # Build the internal schedule string
    internal_schedule = "["
    for n, (heur_name, heur_time) in enumerate(schedule_spec):

        # If this is the last heuristic or the time is None, we run with -1
        if n + 1 >= len(schedule_spec) and (last_call or heur_time is None):
            # This is the last heuristic and should run until timeout
            internal_schedule += "[{0};-1];".format(heur_name)

            # Make sure the timeout is larger than the time left such that it is truncated
            timeout += 2 * (time_left + 1)
        else:
            # Because of support of None for the external schedules, we have to  convert
            # the None into the time_left. This will be truncated in the overall timeout
            if heur_time is None:
                heur_time = time_left

            # Set the internal heuristic
            internal_schedule += "[{0};{1:.0f}];".format(heur_name, heur_time)

            # Increment the timeout with this heuristic
            timeout += heur_time

    # Remove the last semi-colon and append closing bracket
    if len(internal_schedule) > 2:
        internal_schedule = internal_schedule[:-1]
    internal_schedule += "]"

    # We usually spend 10% of the prover time on preprocessing
    timeout = int(timeout * 1.10)

    # Truncate the timeout, in case it exceeds the time available
    timeout = min(timeout, time_left)

    # Get the prep heuristic and make sure the --schedule options is removed
    prep_heur_string = get_heuristic_string(prep_heuristic, timeout, opt_filter=["schedule"])

    # Build the execution string
    # Set prover, prep heuristic, internal heuristic and timeout
    cmd_string = "{0} {1} --schedule \"{2}\" --time_out_real {3:.2f} {4}".format(
        config.PROVER, prep_heur_string, internal_schedule, timeout, problem)

    return cmd_string, timeout, internal_schedule
